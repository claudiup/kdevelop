# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Phuwanat Sakornsakolpat <narachai@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-28 00:47+0000\n"
"PO-Revision-Date: 2010-05-27 23:07+0700\n"
"Last-Translator: Phuwanat Sakornsakolpat <narachai@gmail.com>\n"
"Language-Team: Thai <thai-l10n@googlegroups.com>\n"
"Language: th\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: outputwidget.cpp:43
#, kde-format
msgctxt "@info:tooltip"
msgid "Enter a case-insensitive regular expression to filter the output view"
msgstr ""

#: outputwidget.cpp:61
#, fuzzy, kde-format
#| msgid "Output View"
msgctxt "@title:window"
msgid "Output View"
msgstr "มุมมองผลลัพธ์"

#: outputwidget.cpp:72
#, fuzzy, kde-format
#| msgid "Close the currently active output view"
msgctxt "@info:tooltip"
msgid "Close the currently active output view"
msgstr "ปิดมุมมองผลลัพธ์ที่ใช้งานอยู่"

#: outputwidget.cpp:78
#, fuzzy, kde-format
#| msgid "Close the currently active output view"
msgctxt "@info:tooltip"
msgid "Close all other output views"
msgstr "ปิดมุมมองผลลัพธ์ที่ใช้งานอยู่"

#: outputwidget.cpp:89
#, fuzzy, kde-format
#| msgid "Jump to Previous Outputmark"
msgctxt "@action"
msgid "Previous Output"
msgstr "ข้ามไปยังจุดผลลัพธ์ที่กำหนดก่อนหน้า"

#: outputwidget.cpp:92
#, fuzzy, kde-format
#| msgid "Jump to Next Outputmark"
msgctxt "@action"
msgid "Next Output"
msgstr "ข้ามไปยังจุดผลลัพธ์ที่กำหนดต่อไป"

#: outputwidget.cpp:97
#, fuzzy, kde-format
#| msgid "Select activated Item"
msgctxt "@action"
msgid "Select Activated Item"
msgstr "เลือกรายการที่เปิดใช้งาน"

#: outputwidget.cpp:99
#, fuzzy, kde-format
#| msgid "Focus when selecting Item"
msgctxt "@action"
msgid "Focus when Selecting Item"
msgstr "โฟกัสเมื่อเลือกรายการ"

#: outputwidget.cpp:113
#, fuzzy, kde-format
#| msgid "Next"
msgctxt "@action"
msgid "First Item"
msgstr "ถัดไป"

#: outputwidget.cpp:117
#, fuzzy, kde-format
#| msgid "Previous"
msgctxt "@action"
msgid "Previous Item"
msgstr "ก่อนหน้า"

#: outputwidget.cpp:121
#, fuzzy, kde-format
#| msgid "Next"
msgctxt "@action"
msgid "Next Item"
msgstr "ถัดไป"

#: outputwidget.cpp:125
#, fuzzy, kde-format
#| msgid "Next"
msgctxt "@action"
msgid "Last Item"
msgstr "ถัดไป"

#: outputwidget.cpp:138
#, kde-format
msgctxt "@action"
msgid "Clear"
msgstr ""

#: outputwidget.cpp:149
#, kde-format
msgctxt "@info:placeholder"
msgid "Search..."
msgstr ""

#: outputwidget.cpp:724
#, kde-format
msgctxt ""
"@info:tooltip %1 - position in the pattern, %2 - textual description of the "
"error"
msgid "Filter regular expression pattern error at offset %1: %2"
msgstr ""

#: standardoutputview.cpp:101
#, fuzzy, kde-format
#| msgid "Build"
msgctxt "@title:window"
msgid "Build"
msgstr "ประกอบสร้าง"

#: standardoutputview.cpp:106
#, fuzzy, kde-format
#| msgid "Run"
msgctxt "@title:window"
msgid "Run"
msgstr "ทำงาน"

#: standardoutputview.cpp:111
#, fuzzy, kde-format
#| msgid "Debug"
msgctxt "@title:window"
msgid "Debug"
msgstr "ดีบัก"

#: standardoutputview.cpp:116
#, fuzzy, kde-format
#| msgid "Test"
msgctxt "@title:window"
msgid "Test"
msgstr "ทดสอบ"

#: standardoutputview.cpp:121
#, fuzzy, kde-format
#| msgid "Version Control"
msgctxt "@title:window"
msgid "Version Control"
msgstr "การควบคุมรุ่น"

#~ msgctxt "@title:menu"
#~ msgid "Navigation"
#~ msgstr "การนำทาง"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ภูวณัฏฐ์ สาครสกลพัฒน์"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "narachai@gmail.com"

#~ msgid "Provides toolviews for presenting the output of running apps"
#~ msgstr "มีมุมมองเครื่องมือสำหรับแสดงผลลัพธ์ของโปรแกรมที่ทำงานอยู่"
