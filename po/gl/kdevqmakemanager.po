# translation of kdevqmake.po to galician
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# mvillarino <mvillarino@users.sourceforge.net>, 2008.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: kdevqmake\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-09 00:15+0000\n"
"PO-Revision-Date: 2017-07-29 20:35+0100\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <kde-i18n-doc@kde.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Adrián Chaves Fernández"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "adriyetichaves@gmail.com"

#: qmakebuilddirchooser.cpp:124
#, kde-format
msgid "Please specify path to QMake executable."
msgstr "Indique a ruta do executábel de QMake."

#: qmakebuilddirchooser.cpp:129
#, kde-format
msgid "QMake executable \"%1\" does not exist."
msgstr "O executábel de QMake indicado, «%1», non existe."

#: qmakebuilddirchooser.cpp:132
#, kde-format
msgid "QMake executable is not a file."
msgstr "O executábel de QMake non é un ficheiro."

#: qmakebuilddirchooser.cpp:135
#, kde-format
msgid "QMake executable is not executable."
msgstr "O executábel de QMake non é un executábel."

#: qmakebuilddirchooser.cpp:140
#, kde-format
msgid "QMake executable cannot be queried for variables."
msgstr "Non se poden obter variábeis do executábel de QMake."

#: qmakebuilddirchooser.cpp:143
#, kde-format
msgid "No basic MkSpec file could be found for the given QMake executable."
msgstr ""
"Non foi posíbel atopar un ficheiro MkSpec básico para o executábel de QMake "
"indicado."

#: qmakebuilddirchooser.cpp:150
#, kde-format
msgid "Please specify a build folder."
msgstr "Indique un cartafol de construción."

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: qmakebuilddirchooser.ui:23
#, fuzzy, kde-format
#| msgid "QMake &executable:"
msgctxt "@label:textbox"
msgid "QMake &executable:"
msgstr "O &executábel de QMake:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: qmakebuilddirchooser.ui:46
#, fuzzy, kde-format
#| msgid "&Build directory:"
msgctxt "@label:textbox"
msgid "&Build directory:"
msgstr "&Directorio de construción:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: qmakebuilddirchooser.ui:62
#, fuzzy, kde-format
#| msgid "&Install to (target.path):"
msgctxt "@label:textbox"
msgid "&Install to (target.path):"
msgstr "&Instalar en (ruta):"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: qmakebuilddirchooser.ui:85
#, fuzzy, kde-format
#| msgid "Build &type:"
msgctxt "@labal:listbox"
msgid "Build &type:"
msgstr "&Tipo de construción:"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_buildType)
#: qmakebuilddirchooser.ui:115
#, fuzzy, kde-format
#| msgid "(Use default from .pro file)"
msgctxt "@item:inlistbox"
msgid "(Use default from .pro file)"
msgstr "(Usar o predeterminado do ficheiro «.pro»)"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: qmakebuilddirchooser.ui:123
#, fuzzy, kde-format
#| msgid "Extra arguments:"
msgctxt "@label:textbox"
msgid "Extra arguments:"
msgstr "Argumentos adicionais:"

#: qmakebuilddirchooserdialog.cpp:22
#, fuzzy, kde-format
#| msgid "Configure a build directory"
msgctxt "@title:window"
msgid "Configure a Build Directory"
msgstr "Configurar un directorio de construción"

#: qmakejob.cpp:33
#, kde-format
msgid "Run QMake in %1"
msgstr "Executar QMake en %1"

#: qmakejob.cpp:137
#, kde-format
msgid "*** Exited with return code: %1 ***"
msgstr "*** Saíu co código de retorno: %1 ***"

#: qmakejob.cpp:139
#, kde-format
msgid "*** Process aborted ***"
msgstr "*** Interrompeuse o proceso ***"

#: qmakejob.cpp:141
#, kde-format
msgid "*** Crashed with return code: %1 ***"
msgstr "*** Quebrou co código de retorno: %1 ***"

#: qmakemanager.cpp:75
#, fuzzy, kde-format
#| msgid "Run QMake"
msgctxt "@action"
msgid "Run QMake"
msgstr "Executar QMake"

#~ msgid "QMake &binary:"
#~ msgstr "Executá&bel de QMake:"

#~ msgid "qmake-parser"
#~ msgstr "analisador de qmake"

#~ msgid "Parse QMake project files"
#~ msgstr "Procesa ficheiros de proxecto de QMake"

#~ msgid "Enable output of the debug AST"
#~ msgstr "Deshabilitar a saída do AST de depuración"

#~ msgid "QMake project files"
#~ msgstr "Ficheiros de proxecto de QMake"

#~ msgid "QMake Manager"
#~ msgstr "Xestor de QMake"

#~ msgid "Support for managing QMake projects"
#~ msgstr "Soporta a xestión de proxectos con QMake"
